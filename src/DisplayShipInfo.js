import React, { Component } from 'react';

class DisplayShipInfo extends Component {
    render() {
        const {
            id,    
            shipModels,
        } = this.props;
    
            return (
                id  === 0 || id === 1 || id === 2 ? 
                <ul>
                    <p>Selected ship: {shipModels[id].title}</p> 
                    <p>Number of ship slots: {shipModels[id].size}</p>
                </ul>
                :
                <div>

                </div>
            );
    }

}
export default DisplayShipInfo;