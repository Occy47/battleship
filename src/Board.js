import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

class Board extends Component {
  render() {
    const {
      xAxis,
      yAxis,
      board,
      clickShipField,
      selectedShipSlot,
    } = this.props;

    return (
      <div className="board">
        <div className="row">
          <span>
            <button className="field-description">bsm</button>
            {xAxis.map((x, ix) => <button key={ix} className="field-description">{x}</button>)}
          </span>
        </div>
        {board.map((b, ix) => (
          <div className="row">
            <span>
              <button key={`b${ix}`} className="field-description">{yAxis[ix]}</button>
              {b.map((x, ixChild) => <Field x={ix}
                y={ixChild}
                key={`f${ix}${ixChild}`}
                isClicked={x}
                onFieldClick={clickShipField}
                selectedShipSlot={selectedShipSlot} />)}
            </span>
          </div>
        ))}
      </div>
    );
  }
}

const ShipImg = ({ selectedImg }) => (
  <img className="ship-btn-dim" src={selectedImg} alt="bow" />
);

const Field = ({ x, y, isClicked, onFieldClick, selectedShipSlot }) => (
  <Button variant="contained" color="primary" className="btn-dim" onClick={() => onFieldClick(x, y)}>
    {isClicked ? <ShipImg selectedImg={selectedShipSlot} /> : "~"}
  </Button>
);

export default Board;