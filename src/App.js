import React, { Component } from 'react';
import Game from '../src/Game';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <h1>BattleShip Madness</h1>
          <div>
              <Game />
          </div>
        </div>

      </div>
    );
  }
}

export default App;
