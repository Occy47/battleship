import React from 'react';
import Switch from '@material-ui/core/Switch';

class ModeSwitch extends React.Component {

  defaultChecked = this.props;
  selectMode = this.props;

  render() {
    const {defaultChecked, selectMode} = this.props;
    return (
      <div>
        <Switch onClick={() => selectMode} checked={defaultChecked} value="checkedF" color="default" />
      </div>
    );
  }
}

export default ModeSwitch;