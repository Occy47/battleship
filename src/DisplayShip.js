import React, { Component } from 'react';

class DisplayShip extends Component {
    render() {
        const {
            id,
            shipModels,
            handleSelectedShipSlot,
        } = this.props;

            return(
                id  === 0 || id === 1 || id === 2 ? 
                <span>
                {shipModels[id].images.map((ship, i) => <ShipImg
                        imgUrl={ship}
                        key={i}
                        handleSelectedShipSlot={handleSelectedShipSlot}

                    />) 
                }
                </span>
                :
                <div>
                No ships selected
                </div>
            );
        }
}
const ShipImg = ({ imgUrl, handleSelectedShipSlot, }) => (
    <img className="ship-btn-dim" src={imgUrl} onClick={() => handleSelectedShipSlot({ imgUrl })} alt="shipSection" />
);


export default DisplayShip;