import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DisplayShip from '../src/DisplayShip'
import DisplayShipInfo from './DisplayShipInfo'


const styles = theme => ({
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
});

class SelectMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shipId: '',
            open: false,
        };
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    render() {
        const {
            classes, 
            deployModeText,
            shipModels,
            handleSelectedShipSlot,
        } = this.props;
        return (
            <div>
                <h3>
                    {deployModeText ? "Plese select ship: " : "Play"}
                </h3>
                <form autoComplete="off">
                        <FormControl className={classes.formControl} disabled={!deployModeText} >
                            <InputLabel >Ship size</InputLabel>
                            <Select
                                open={this.state.open}
                                onClose={this.handleClose}
                                onOpen={this.handleOpen}
                                value={this.state.shipId}
                                onChange={this.handleChange}
                                inputProps={{
                                    name: 'shipId',
                                    id: 'interactions-open-select',
                                }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={shipModels[0].objectId}>{shipModels[0].title}</MenuItem>
                                <MenuItem value={shipModels[1].objectId}>{shipModels[1].title}</MenuItem>
                                <MenuItem value={shipModels[2].objectId}>{shipModels[2].title}</MenuItem>
                            </Select>
                        </FormControl>
                </form>
                <div>
                     <DisplayShipInfo id={this.state.shipId} shipModels={shipModels}/> 
                </div>
                <div>
                    <DisplayShip id={this.state.shipId} shipModels={shipModels} handleSelectedShipSlot={handleSelectedShipSlot} />
                </div>
            </div>

        );
    }
}

SelectMenu.propTypes = {
    classes: PropTypes.object.isRequired,

};

export default withStyles(styles)(SelectMenu);

