import React, { Component } from 'react';
import Board from '../src/Board';
import SelectMenu from './SelectMenu';
import Button from '@material-ui/core/Button';
import shipBow from './img/ship_bow.jpg';
import shipMiddle from './img/ship_middle.jpg';
import shipStern from './img/ship_stern.jpg';

class Game extends Component {

    constructor(props) {
        super(props);
        this.state = {
            xAxis: [1, 2, 3, 4, 5, 6, 7, 8],
            yAxis: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
            board: [
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false],
                [false, false, false, false, false, false, false, false]
            ],
            deployMode: true,
            selectedShipSlot: shipMiddle,
            shipModels: [
                {
                    objectId: 0,
                    title: "Small ship",
                    size: 2,
                    images: [shipBow, shipStern],
                },
                {
                    objectId: 1,
                    title: "Medium ship",
                    size: 3,
                    images: [shipBow, shipMiddle, shipStern],
                },
                {
                    objectId: 2,
                    title: "Large ship",
                    size: 5,
                    images: [shipBow, shipMiddle, shipMiddle, shipMiddle, shipStern],
                }
            ],
        };
        this.clickShipField = this.clickShipField.bind(this);
        this.handleMode = this.handleMode.bind(this);
        this.handleSelectedShipSlot = this.handleSelectedShipSlot.bind(this);
    }

    clickShipField(x, y) {
        const { board } = this.state;
        var updatedBoard = board.map(x => x.slice());
        updatedBoard[x][y] = true;

        this.setState({
            ...this.state,
            board: updatedBoard,
        });
    }

    handleMode = () => {
        const { deployMode } = this.state;
        var toggleMode;

        deployMode ?
            toggleMode = false :
            toggleMode = true

        this.setState({
            deployMode: toggleMode
        });
    }

    handleSelectedShipSlot(imgUrl) {
        const newShipSlot = imgUrl;
        
        this.setState({
            selectedShipSlot: newShipSlot,
        });
    }

    render() {

        console.log(this.state.selectedShipSlot);

        return (
            <div>
                <span>
                    <div>
                        <Board xAxis={this.state.xAxis}
                            yAxis={this.state.yAxis}
                            board={this.state.board}
                            clickShipField={this.clickShipField}
                            selectedShipSlot={this.state.selectedShipSlot} />
                    </div>
                    <div className='interactions'>
                        <Button onClick={this.handleMode}>Mode</Button>
                        <SelectMenu deployModeText={this.state.deployMode}
                            shipModels={this.state.shipModels}
                            handleSelectedShipSlot={this.handleSelectedShipSlot} />
                    </div>
                </span>
            </div>
        );
    }
}



export default Game;
